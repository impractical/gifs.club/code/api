package api

import (
	"errors"
	"net/http"
	"time"

	"darlinggo.co/api"
	"darlinggo.co/trout"
	"gifs.club/code/metadata"
	uuid "github.com/hashicorp/go-uuid"
	yall "yall.in"
)

var (
	ErrLastUploadAtAfterInvalidFormat  = errors.New("invalid format for lastUploadAtAfter")
	ErrLastUploadAtBeforeInvalidFormat = errors.New("invalid format for lastUploadAtBefore")
)

type Collection struct {
	ID           string    `json:"id"`
	Slug         string    `json:"slug"`
	Name         string    `json:"name"`
	Description  string    `json:"description"`
	CreatedAt    time.Time `json:"createdAt"`
	LastUploadAt time.Time `json:"lastUploadAt"`
	Owners       []string  `json:"ownerIDs"`
}

func metadataCollection(c Collection) metadata.Collection {
	return metadata.Collection{
		ID:           c.ID,
		Slug:         c.Slug,
		Name:         c.Name,
		Description:  c.Description,
		CreatedAt:    c.CreatedAt,
		LastUploadAt: c.LastUploadAt,
		Owners:       c.Owners,
	}
}

func apiCollection(c metadata.Collection) Collection {
	return Collection{
		ID:           c.ID,
		Slug:         c.Slug,
		Name:         c.Name,
		Description:  c.Description,
		CreatedAt:    c.CreatedAt,
		LastUploadAt: c.LastUploadAt,
		Owners:       c.Owners,
	}
}

func metadataCollections(cs []Collection) []metadata.Collection {
	res := make([]metadata.Collection, 0, len(cs))
	for _, c := range cs {
		res = append(res, metadataCollection(c))
	}
	return res
}

func apiCollections(cs []metadata.Collection) []Collection {
	res := make([]Collection, 0, len(cs))
	for _, c := range cs {
		res = append(res, apiCollection(c))
	}
	return res
}

func (s Service) handleCollectionsList(w http.ResponseWriter, r *http.Request) {
	log := yall.FromContext(r.Context())
	filter, err := s.collectionFilterFromRequest(r)
	if err != nil {
		var e api.RequestError
		if err == ErrLastUploadAtAfterInvalidFormat {
			e.Param = "lastUploadAtAfter"
			e.Slug = api.RequestErrInvalidFormat
		} else if err == ErrLastUploadAtBeforeInvalidFormat {
			e.Param = "lastUploadAtBefore"
			e.Slug = api.RequestErrInvalidFormat
		} else {
			e.Slug = api.RequestErrActOfGod
		}
		api.Encode(w, r, http.StatusBadRequest, Response{Errors: []api.RequestError{e}})
		return
	}
	collections, err := s.Collections.ListCollections(r.Context(), filter)
	if err != nil {
		log.WithError(err).Error("error listing collections")
		api.Encode(w, r, http.StatusInternalServerError, Response{Errors: api.ActOfGodError})
		return
	}
	api.Encode(w, r, http.StatusOK, Response{Collections: apiCollections(collections)})
}

func (s Service) handleCollectionGet(w http.ResponseWriter, r *http.Request) {
	log := yall.FromContext(r.Context())
	id := trout.RequestVars(r).Get("collection")
	if id == "" {
		api.Encode(w, r, http.StatusNotFound, Response{Errors: []api.RequestError{{Param: "collection", Slug: api.RequestErrMissing}}})
		return
	}
	log = log.WithField("collection", id)
	collection, err := s.Collections.GetCollection(r.Context(), id)
	if err != nil {
		if err == metadata.ErrCollectionNotFound {
			api.Encode(w, r, http.StatusNotFound, Response{Errors: []api.RequestError{{Param: "collection", Slug: api.RequestErrNotFound}}})
			return
		}
		log.WithError(err).Error("error retrieving collection")
		api.Encode(w, r, http.StatusInternalServerError, Response{Errors: api.ActOfGodError})
		return
	}
	api.Encode(w, r, http.StatusOK, Response{Collections: []Collection{apiCollection(collection)}})
}

func (s Service) handleCollectionUpdate(w http.ResponseWriter, r *http.Request) {
}

func (s Service) handleCollectionDelete(w http.ResponseWriter, r *http.Request) {
}

func (s Service) handleCollectionCreate(w http.ResponseWriter, r *http.Request) {
	log := yall.FromContext(r.Context())

	// TODO: check that the user is authenticated
	var collection Collection
	err := api.Decode(r, &collection)
	if err != nil {
		log.WithError(err).Error("error parsing collection")
		api.Encode(w, r, http.StatusBadRequest, Response{Errors: api.InvalidFormatError})
		return
	}
	collection.ID, err = uuid.GenerateUUID()
	if err != nil {
		log.WithError(err).Error("error generating uuid")
		api.Encode(w, r, http.StatusInternalServerError, Response{Errors: api.ActOfGodError})
		return
	}

	var reqErrs []api.RequestError
	if collection.Slug == "" {
		reqErrs = append(reqErrs, api.RequestError{Field: "/slug", Slug: api.RequestErrMissing})
	}
	if collection.Name == "" {
		reqErrs = append(reqErrs, api.RequestError{Field: "/name", Slug: api.RequestErrMissing})
	}
	if len(reqErrs) > 0 {
		api.Encode(w, r, http.StatusBadRequest, Response{Errors: reqErrs})
		return
	}
	collection.CreatedAt = time.Now()
	collection.Owners = []string{} // TODO: set to request author
	mColl := metadataCollection(collection)
	err = s.Collections.CreateCollection(r.Context(), mColl)
	if err != nil {
		if err == metadata.ErrCollectionAlreadyExists {
			api.Encode(w, r, http.StatusBadRequest, Response{Errors: []api.RequestError{{Slug: api.RequestErrConflict, Field: "/id"}}})
			return
		}
		if err == metadata.ErrCollectionSlugAlreadyExists {
			api.Encode(w, r, http.StatusBadRequest, Response{Errors: []api.RequestError{{Slug: api.RequestErrConflict, Field: "/slug"}}})
			return
		}
		log.WithError(err).Error("error storing collection metadata")
		api.Encode(w, r, http.StatusInternalServerError, Response{Errors: api.ActOfGodError})
		return
	}
	api.Encode(w, r, http.StatusCreated, Response{Collections: []Collection{collection}})
}

func (s Service) collectionFilterFromRequest(r *http.Request) (metadata.CollectionFilter, error) {
	res := metadata.CollectionFilter{
		IDs:    r.URL.Query()["id"],
		Slugs:  r.URL.Query()["slug"],
		Owners: r.URL.Query()["owners"],
	}
	if r.URL.Query().Get("lastUploadAtAfter") != "" {
		upAt, err := time.Parse(r.URL.Query().Get("lastUploadAtAfter"), time.RFC3339)
		if err != nil {
			yall.FromContext(r.Context()).WithError(err).WithField("value",
				r.URL.Query().Get("lastUploadAtAfter")).Debug("error parsing lastUploadAtAfter")
			return res, ErrLastUploadAtAfterInvalidFormat
		}
		res.LastUploadAtAfter = &upAt
	}
	if r.URL.Query().Get("lastUploadAtBefore") != "" {
		upAt, err := time.Parse(r.URL.Query().Get("lastUploadAtBefore"), time.RFC3339)
		if err != nil {
			yall.FromContext(r.Context()).WithError(err).WithField("value",
				r.URL.Query().Get("lastUploadAtBefore")).Debug("error parsing lastUploadAtBefore")
			return res, ErrLastUploadAtBeforeInvalidFormat
		}
		res.LastUploadAtBefore = &upAt
	}
	return res, nil
}
