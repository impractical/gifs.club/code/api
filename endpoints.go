package api

import (
	"net/http"
	"strings"

	"darlinggo.co/trout"
	yall "yall.in"
)

func parseTraceHeaders(r *http.Request) (traceID, spanID string) {
	h := strings.TrimSpace(r.Header.Get("X-Cloud-Trace-Context"))
	if h == "" {
		return "", ""
	}
	parts := strings.Split(h, "/")
	if len(parts) != 2 {
		return "", ""
	}
	traceID = parts[0]
	parts = strings.Split(parts[1], ";")
	spanID = parts[0]
	return traceID, spanID
}

func (s Service) contextLogger(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log := s.Log.WithRequest(r).WithField("endpoint", r.Header.Get("Trout-Pattern")).WithField("method", r.Method)
		for k, v := range trout.RequestVars(r) {
			log = log.WithField("url."+strings.ToLower(k), v)
		}
		traceID, spanID := parseTraceHeaders(r)
		if traceID != "" && spanID != "" {
			log = log.WithField("trace_id", traceID).WithField("span_id", spanID)
		}
		r = r.WithContext(yall.InContext(r.Context(), log))
		log.Debug("serving request")
		h.ServeHTTP(w, r)
	})
}

// Server returns an http.Handler that will handle all
// the requests for v1 of the API. The baseURL should be
// set to whatever prefix the muxer matches to pass requests
// to the Handler; consider it the root path of v1 of the API.
func (s Service) Server(baseURL string) http.Handler {
	var router trout.Router
	router.SetPrefix(baseURL)

	router.Endpoint("/collections").Methods("POST").Handler(s.contextLogger(http.HandlerFunc(s.handleCollectionCreate)))
	router.Endpoint("/collections").Methods("GET").Handler(s.contextLogger(http.HandlerFunc(s.handleCollectionsList)))
	router.Endpoint("/collections/{collection}").Methods("GET").Handler(s.contextLogger(http.HandlerFunc(s.handleCollectionGet)))
	router.Endpoint("/collections/{collection}").Methods("PATCH").Handler(s.contextLogger(http.HandlerFunc(s.handleCollectionUpdate)))
	router.Endpoint("/collections/{collection}").Methods("DELETE").Handler(s.contextLogger(http.HandlerFunc(s.handleCollectionDelete)))

	router.Endpoint("/collections/{collection}/upload").Methods("POST").Handler(s.contextLogger(http.HandlerFunc(s.handleUpload)))
	router.Endpoint("/collections/{collection}/gifs").Methods("POST").Handler(s.contextLogger(http.HandlerFunc(s.handleGIFCreate)))
	router.Endpoint("/collections/{collection}/gifs").Methods("GET").Handler(s.contextLogger(http.HandlerFunc(s.handleGIFsList)))
	router.Endpoint("/collections/{collection}/gifs/{gif}").Methods("PATCH").Handler(s.contextLogger(http.HandlerFunc(s.handleGIFUpdate)))
	router.Endpoint("/collections/{collection}/gifs/{gif}").Methods("DELETE").Handler(s.contextLogger(http.HandlerFunc(s.handleGIFDelete)))
	router.Endpoint("/collections/{collection}/gifs/{gif}").Methods("GET").Handler(s.contextLogger(http.HandlerFunc(s.handleGIFGet)))

	router.Endpoint("/collections/{collection}/gifs/{gif}/process").Methods("POST").Handler(s.contextLogger(http.HandlerFunc(s.handleReprocess)))
	router.Endpoint("/collections/{collection}/gifs/{gif}/download").Methods("GET").Handler(s.contextLogger(http.HandlerFunc(s.handleDownload)))

	return router
}
