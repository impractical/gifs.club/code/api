package api

import (
	"time"

	"gifs.club/code/metadata"
)

type File struct {
	Hash             string    `json:"sha256"`
	Extension        string    `json:"extension"`
	ContentType      string    `json:"contentType"`
	FileSize         int64     `json:"fileSize"`
	PlaceholderColor string    `json:"placeholderColor"`
	PlaceholderBytes []byte    `json:"placeholderBytes,omitempty"`
	CreatedAt        time.Time `json:"createdAt,omitempty"`
	FirstUploadedBy  string    `json:"firstUploadedBy,omitempty"`
}

func apiFile(f metadata.File) File {
	return File{
		Hash:             f.Hash,
		Extension:        f.Extension,
		ContentType:      f.ContentType,
		FileSize:         f.FileSize,
		PlaceholderColor: f.PlaceholderColor,
		PlaceholderBytes: f.PlaceholderBytes,
		CreatedAt:        f.CreatedAt,
		FirstUploadedBy:  f.FirstUploadedBy,
	}
}

func metadataFile(f File) metadata.File {
	return metadata.File{
		Hash:             f.Hash,
		Extension:        f.Extension,
		ContentType:      f.ContentType,
		FileSize:         f.FileSize,
		PlaceholderColor: f.PlaceholderColor,
		PlaceholderBytes: f.PlaceholderBytes,
		CreatedAt:        f.CreatedAt,
		FirstUploadedBy:  f.FirstUploadedBy,
	}
}
