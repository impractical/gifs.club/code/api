package api

import (
	"context"
	"encoding/base64"
	"encoding/binary"
	"fmt"
	"io"
	"io/ioutil"
	"mime"
	"mime/multipart"
	"net/http"
	"path"
	"sort"
	"strings"
	"time"

	"darlinggo.co/api"
	"darlinggo.co/trout"
	"gifs.club/code/metadata"
	"gifs.club/code/storage"
	uuid "github.com/hashicorp/go-uuid"
	"gitlab.com/paddycarver/magic-number-checker/checker"
	yall "yall.in"
)

type GIF struct {
	ID         string    `json:"id"`
	Collection string    `json:"collectionID"`
	Path       string    `json:"path"`
	FileHash   string    `json:"hash"`
	CreatedAt  time.Time `json:"createdAt,omitempty"`
	CreatedBy  string    `json:"createdBy"`
}

func apiGIF(g metadata.GIF) GIF {
	return GIF{
		ID:         g.ID,
		Collection: g.Collection,
		Path:       g.Path,
		FileHash:   g.FileHash,
		CreatedAt:  g.CreatedAt,
		CreatedBy:  g.CreatedBy,
	}
}

type uploadParts struct {
	image     io.ReadCloser
	sha256    string
	path      string
	extension string
	crc32c    uint32
}

func (s Service) parseUploadPart(ctx context.Context, part *multipart.Part, up uploadParts) (uploadParts, Response) {
	log := yall.FromContext(ctx).WithField("part", part.FormName())
	switch part.FormName() {
	case "crc32c":
		body, err := ioutil.ReadAll(part)
		if err != nil {
			log.WithError(err).Error("error reading part")
			return up, Response{Code: http.StatusInternalServerError, Errors: api.ActOfGodError}
		}
		crcStr, err := base64.StdEncoding.DecodeString(string(body))
		if err != nil {
			return up, Response{Code: http.StatusBadRequest, Errors: []api.RequestError{{Param: "crc32c", Slug: api.RequestErrInvalidFormat}}}
		}
		up.crc32c = binary.BigEndian.Uint32([]byte(crcStr))
	case "sha256":
		body, err := ioutil.ReadAll(part)
		if err != nil {
			return up, Response{Code: http.StatusInternalServerError, Errors: api.ActOfGodError}
		}
		up.sha256 = string(body)
	case "path":
		body, err := ioutil.ReadAll(part)
		if err != nil {
			return up, Response{Code: http.StatusInternalServerError, Errors: api.ActOfGodError}
		}
		up.path = string(body)
		up.extension = path.Ext(up.path)
		up.path = up.path[0 : len(up.path)-len(path.Ext(up.path))]
	case "gif":
		up.image = part
	}
	return up, Response{}
}

func (s Service) handleUpload(w http.ResponseWriter, r *http.Request) {
	log := yall.FromContext(r.Context())

	// TODO: check that the user is authenticated

	collection := trout.RequestVars(r).Get("collection")
	// TODO: check that the user has upload privileges for the collection

	// get the form data
	// multipart forms retain order, so we can rely on the image data being set last
	// this allows us to avoid reading it into memory, and lets us do a streaming upload
	mediaType, params, err := mime.ParseMediaType(r.Header.Get("Content-Type"))
	if err != nil {
		api.Encode(w, r, http.StatusBadRequest, Response{Errors: []api.RequestError{{Header: "Content-Type", Slug: api.RequestErrInvalidFormat}}})
		return
	}
	if !strings.HasPrefix(mediaType, "multipart/") {
		api.Encode(w, r, http.StatusBadRequest, Response{Errors: []api.RequestError{{Header: "Content-Type", Slug: api.RequestErrInvalidValue}}})
		return
	}
	multiReader := multipart.NewReader(r.Body, params["boundary"])
	var up uploadParts
	for {
		part, err := multiReader.NextPart()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.WithError(err).WithField("error_type", fmt.Sprintf("%T", err)).Error("error reading next part of form")
			api.Encode(w, r, http.StatusBadRequest, Response{Errors: api.InvalidFormatError})
			return
		}
		var resp Response
		up, resp = s.parseUploadPart(r.Context(), part, up)
		if resp.Code != 0 {
			api.Encode(w, r, resp.Code, resp)
			return
		}
		// we need the image to be the last part we process, or the reader will get recycled and we'll lose the data.
		if up.image != nil {
			break
		}
	}

	// any of our required parameters missing should be reported back and the request should fail
	var reqErrs []api.RequestError
	if up.crc32c == 0 {
		reqErrs = append(reqErrs, api.RequestError{Param: "crc32c", Slug: api.RequestErrMissing})
	}
	if up.sha256 == "" {
		reqErrs = append(reqErrs, api.RequestError{Param: "sha256", Slug: api.RequestErrMissing})
	}
	if up.path == "" {
		reqErrs = append(reqErrs, api.RequestError{Param: "path", Slug: api.RequestErrMissing})
	}
	if up.image == nil {
		reqErrs = append(reqErrs, api.RequestError{Param: "gif", Slug: api.RequestErrMissing})
	}
	if len(reqErrs) > 0 {
		api.Encode(w, r, http.StatusBadRequest, Response{Errors: reqErrs})
		return
	}

	// upload the file to our blob storage
	hash := storage.Hashes{
		SHA256: up.sha256,
		CRC32C: up.crc32c,
	}
	log = log.WithField("path", up.path)
	log = log.WithField("sha256", up.sha256)
	log = log.WithField("crc32c", up.crc32c)
	uploadedFile, err := storage.Upload(r.Context(), storage.Dependencies{
		Storer: s.Storage,
	}, hash, up.image)
	if err != nil {
		if err == checker.ErrUnsupportedFile {
			api.Encode(w, r, http.StatusBadRequest, Response{Errors: []api.RequestError{{Slug: api.RequestErrInvalidFormat, Param: "gif"}}})
			return
		}
		log.WithError(err).Error("error uploading file")
		api.Encode(w, r, http.StatusInternalServerError, Response{Errors: api.ActOfGodError})
		return
	}

	// we want the File and GIF to have the same created timestamp if we need to create both
	created := time.Now()

	// if an empty storage.File is returned, it means the file already exists in our blob storage
	var file metadata.File
	if uploadedFile.SHA256 != "" {
		// persist the file metadata
		// a background task fills in the placeholder color and bytes
		var extensions []string
		if up.extension != "" {
			extensions = append(extensions, up.extension)
		}
		mimeExtensions, err := mime.ExtensionsByType(uploadedFile.ContentType)
		if err != nil {
			log.WithError(err).WithField("mime_type", uploadedFile.ContentType).Error("Error getting extensions for content type")
		}
		sort.Strings(mimeExtensions)
		extensions = append(extensions, mimeExtensions...)
		if len(extensions) < 1 {
			log.WithField("mime_type", uploadedFile.ContentType).Error("No extension found for MIME type")
			extensions = []string{"unknown"}
		}
		file = metadata.File{
			Hash:            hash.SHA256,
			ContentType:     uploadedFile.ContentType,
			Extension:       extensions[0],
			FileSize:        uploadedFile.Size,
			CreatedAt:       created,
			FirstUploadedBy: "", //TODO: user ID
		}
		err = s.GIFs.CreateFile(r.Context(), file)
		if err != nil && err != metadata.ErrFileAlreadyExists {
			log.WithError(err).Error("error storing file metadata")
			api.Encode(w, r, http.StatusInternalServerError, Response{Errors: api.ActOfGodError})
			return
		}
		// TODO: kick off some background processing for the File
		// we'll need to set its placeholders (color, bytes, etc.) and convert it into a slew of formats
	}

	// persist the GIF metadata
	id, err := uuid.GenerateUUID()
	if err != nil {
		log.WithError(err).Error("error generating ID")
		api.Encode(w, r, http.StatusInternalServerError, Response{Errors: api.ActOfGodError})
		return
	}
	gif := metadata.GIF{
		ID:         id,
		Collection: collection,
		Path:       up.path,
		FileHash:   hash.SHA256,
		CreatedAt:  created,
		CreatedBy:  "", // TODO: user ID
	}
	err = s.GIFs.CreateGIF(r.Context(), gif)
	if err != nil {
		if err == metadata.ErrGIFPathAlreadyExists {
			api.Encode(w, r, http.StatusBadRequest, Response{Errors: []api.RequestError{{Slug: api.RequestErrConflict, Param: "path"}}})
			return
		}
		log.WithError(err).Error("error storing GIF metadata")
		api.Encode(w, r, http.StatusInternalServerError, Response{Errors: api.ActOfGodError})
		return
	}

	// let the client know what we created
	resp := Response{
		GIFs: []GIF{apiGIF(gif)},
	}
	api.Encode(w, r, http.StatusCreated, resp)
}

func (s Service) handleDownload(w http.ResponseWriter, r *http.Request) {
}

func (s Service) handleReprocess(w http.ResponseWriter, r *http.Request) {
}

// for creating an alias of an already uploaded GIF
func (s Service) handleGIFCreate(w http.ResponseWriter, r *http.Request) {
}

func (s Service) handleGIFUpdate(w http.ResponseWriter, r *http.Request) {
}

func (s Service) handleGIFDelete(w http.ResponseWriter, r *http.Request) {
}

func (s Service) handleGIFsList(w http.ResponseWriter, r *http.Request) {
}

func (s Service) handleGIFGet(w http.ResponseWriter, r *http.Request) {
}

func (s Service) gifFilterFromRequest(r *http.Request) (metadata.GIFFilter, error) {
	return metadata.GIFFilter{}, nil
}
