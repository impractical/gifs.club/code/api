module gifs.club/code/api

replace (
	// TODO: update these to actual releases
	gifs.club/code/metadata v0.0.0 => ../metadata
	gifs.club/code/storage v0.0.0 => ../storage
)

require (
	bitbucket.org/ww/goautoneg v0.0.0-20120707110453-75cd24fc2f2c // indirect
	darlinggo.co/api v0.0.0-20160924005218-06eb95038fc2
	darlinggo.co/trout v1.0.1
	gifs.club/code/metadata v0.0.0
	gifs.club/code/storage v0.0.0
	github.com/google/uuid v1.0.0 // indirect
	github.com/hashicorp/go-uuid v1.0.0
	github.com/pborman/uuid v0.0.0-20180906182336-adf5a7427709 // indirect
	gitlab.com/paddycarver/magic-number-checker v0.0.1
	yall.in v0.0.1
)
