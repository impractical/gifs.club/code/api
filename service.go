package api

import (
	"gifs.club/code/metadata"
	"gifs.club/code/storage"
	yall "yall.in"
)

type Service struct {
	Log         *yall.Logger
	Storage     storage.Storer
	GIFs        metadata.GIFStorer
	Collections metadata.CollectionStorer
}
